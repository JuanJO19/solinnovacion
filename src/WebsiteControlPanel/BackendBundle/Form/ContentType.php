<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Doctrine\ORM\EntityRepository;

class ContentType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder 
                ->add('content', 'textarea', array('required' => false))    
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\Content'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'backend_bundle_content';
    }

}
