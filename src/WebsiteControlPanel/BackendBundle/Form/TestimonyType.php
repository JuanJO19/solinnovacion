<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Doctrine\ORM\EntityRepository;

class TestimonyType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder 
                ->add('mensaje', 'textarea', array('required' => true,
                    'label' => '',
                    'attr' => array('minlength' => 5, 'maxlength' => 144)))
                ->add('nombreCompleto', 'text', array('required' => true,
                    'label' => '',
                    'attr' => array('minlength' => 2, 'maxlength' => 30)))
                ->add('email', 'text', array('required' => true))                 
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\Testimony'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'backend_bundle_testimony';
    }

}
