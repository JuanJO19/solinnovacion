<?php

namespace WebsiteControlPanel\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Doctrine\ORM\EntityRepository;

class WorkerType extends AbstractType {   

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder 
                ->add('description', 'textarea', array('required' => false))
                ->add('name', 'text', array('required' => true,
                    'label' => '',
                    'attr' => array('minlength' => 2, 'maxlength' => 40)))
                ->add('position', 'text', array('required' => true))
                 ->add('imagenPath', 'text', array('required' => false, 'label' => 'Imagen'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebsiteControlPanel\BackendBundle\Entity\Worker'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'backend_bundle_worker';
    }

}
