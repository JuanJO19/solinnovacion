<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Form\ImageType;
use WebsiteControlPanel\BackendBundle\Entity\Worker;
use WebsiteControlPanel\BackendBundle\Form\WorkerType;
use WebsiteControlPanel\BackendBundle\Form\ContentWorkerType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use WebsiteControlPanel\BackendBundle\Entity\ContentWorker;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * workers controller.
 * @author Juan José Molina <jmolina@kijho.com>
 * @author Juan Carlos Galvis<jgalvis@kijho.com>
 */
class WorkerController extends Controller {

    /**
     * Lists all Workers entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:Worker')->findAll())->getResult();

        return $this->render('BackendBundle:Worker:index.html.twig', array(
                    'entities' => $entities,
                    'paginator' => $paginator,
        ));
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Worker $entity) {
        $form = $this->createForm(new WorkerType(), $entity, array(
            'action' => $this->generateUrl('worker_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction() {
        $entity = new Worker();
        $form = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Worker:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function saveRegisterAction(Request $request) {


        $em = $this->getDoctrine()->getManager();
        $entity = new Worker();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('worker', array()));
        }

        return $this->render('BackendBundle:Worker:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find worker entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('BackendBundle:Worker:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Service entity.
     *
     * @param Service $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Worker $entity) {
        $form = $this->createForm(new WorkerType(), $entity, array(
            'action' => $this->generateUrl('worker_update', array(
                'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing Service entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Worker')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find worker entity.');
        }
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            return $this->redirect($this->generateUrl('worker'));
        }

        return $this->render('BackendBundle:Worker:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     */
    public function viewAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Worker')->find($id);

        return $this->render('BackendBundle:Worker:show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * 
     * @param Request $request
     * @return \WebsiteControlPanel\BackendBundle\Controller\JsonResponse
     * @throws type
     */
    public function deleteAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Worker')->find($id);


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find worker entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));

        return $response;
    }

}
