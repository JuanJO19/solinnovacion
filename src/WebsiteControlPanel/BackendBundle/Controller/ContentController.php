<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Form\ContentType;
use WebsiteControlPanel\BackendBundle\Entity\Content;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * content controller.
 * @author Juan José Molina <jmolina@kijho.com>
 */
class ContentController extends Controller {

    /**
     * Lists all Workers entities.
     *
     */
    public function indexAction() {

        return $this->render('BackendBundle:Content:index.html.twig', array(
        ));
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction($type) {
        $em = $this->getDoctrine()->getManager();
        $x = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => $type));
        if ($x != null) {
            $entity = $x;
        } else {
            $entity = new Entity\Content();
        }
        $valor = $entity->getTextTipo($type);
        $form = $this->createForm(new ContentType(), $entity);

        return $this->render('BackendBundle:Content:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'type' => $valor,
                    'value' => $type,
        ));
    }

    /**
     * 
     * @param Request $request
     * @return type
     */
    public function saveRegisterAction(Request $request, $type) {

        $em = $this->getDoctrine()->getManager();
        $x = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => $type));
        if ($x != null) {
            $entity = $x;
        } else {
            $entity = new Entity\Content();
        }
        $form = $this->createForm(new ContentType(), $entity);
        $form->handleRequest($request);
        $valor = $entity->getTextTipo($type);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setType($type);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('content', array()));
        }

        return $this->render('BackendBundle:Content:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'type' => $valor,
                    'value' => $type,
        ));
    }

}
