<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use WebsiteControlPanel\BackendBundle\Entity as Entity;

/**
 * Raza controller.
 *
 */
class TestimonyController extends Controller {

    /**
     * Lists all Project entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:Testimony')->findBy(array(), array('order' => 'ASC')))->getResult();
//        \Symfony\Component\VarDumper\VarDumper::dump($entities);die();

        return $this->render('BackendBundle:Testimonio:index.html.twig', array(
                    'entities' => $entities,
                    'paginator' => $paginator,
        ));
    }

    public function orderTestiAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $listTesti = $request->request->get('testi');

        $i = 1;
        foreach ($listTesti as $testiId) {
            $testiOrder = $em->getRepository('BackendBundle:Testimony')->find($testiId);
            $testiOrder->setOrder($i);
            $i++;
        }
        $em->flush();
        $response['result'] = '__OK__';
        $response['msg'] = 'Orden actualizado';


        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');
        return $r;
    }

    public function deleteTestiAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Testimony')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimony entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));

        return $response;
    }

    public function successTestiAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Testimony')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimonio entity.');
        }
        $entity->setEstado(1);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));

        return $response;
    }

}
