<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller {

    public function mainAction() {
                return $this->render('BackendBundle:Default:main.html.twig');        
    }
    
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $cantidadTrabajadores = count($em->getRepository('BackendBundle:Worker')->findAll());
        $cantidadPortfolios = count($em->getRepository('BackendBundle:Portfolio')->findAll());
        $cantidadClientes = count($em->getRepository('BackendBundle:Client')->findAll());
        $cantidadTestimonios = count($em->getRepository('BackendBundle:Testimony')->findAll());
        $cantidadContact = count($em->getRepository('BackendBundle:ContactMail')->findBy(array('respuesta'=>null)));
        
        return $this->render('BackendBundle:Default:index.html.twig', array(
                    'canTra' => $cantidadTrabajadores,                    
                    'canPort' => $cantidadPortfolios,                    
                    'canCli' => $cantidadClientes,                    
                    'canTes' => $cantidadTestimonios,                    
                    'canCont' => $cantidadContact,                    
        ));
    }

    public function loginAction() {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR, $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );
        return $this->render('BackendBundle:Default:login.html.twig', array(
                    'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
                    'error' => $error));
    }

}
