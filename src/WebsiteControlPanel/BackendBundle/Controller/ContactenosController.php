<?php

namespace WebsiteControlPanel\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use WebsiteControlPanel\BackendBundle\Entity\RespuestaMail;
use WebsiteControlPanel\BackendBundle\Form\RespuestaContactenosType;
use WebsiteControlPanel\BackendBundle\Entity as Entity;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Raza controller.
 *
 */
class ContactenosController extends Controller {

    /**
     * Lists all Project entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(10);
        $paginator->setMaxPagerItems(4);
        $entities = $paginator->paginate($em->getRepository('BackendBundle:ContactMail')->findAll())->getResult();

        return $this->render('BackendBundle:Contactenos:index.html.twig', array(
                    'entities' => $entities,
                    'paginator' => $paginator,
        ));
    }

    /**
     * Creates a form to create a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Respuesta $entity) {
        $form = $this->createForm(new RespuestaContactenosType(), $entity, array(
            'action' => $this->generateUrl('backend_cotizaciones_respuesta_add', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Responder'));

        return $form;
    }

    /**
     * Displays a form to create a new Project entity.
     *
     */
    public function newAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entitie = $em->getRepository('BackendBundle:ContactMail')->find($id);
        $temp = $entitie->getRespuesta();
        $entity = $temp != null ? $em->getRepository('BackendBundle:RespuestaMail')->find($temp) : new RespuestaMail();
        $form = $this->createForm(new RespuestaContactenosType(), $entity);

        return $this->render('BackendBundle:Contactenos:newRespuesta.html.twig', array(
                    'entity' => $entity,
                    'entitie' => $entitie,
                    'form' => $form->createView(),
        ));
    }

    public function saveRespuestaAction(Request $request, $id) {


        $em = $this->getDoctrine()->getManager();
        $entity = new RespuestaMail();
        $form = $this->createForm(new RespuestaContactenosType(), $entity);
        $form->handleRequest($request);
        $entitie = $em->getRepository('BackendBundle:ContactMail')->find($id);

        if ($form->isValid()) {
            
            $em->persist($entity);
            $entitie->setRespuesta($entity);            
            $em->persist($entitie);
            $em->flush();

            $message = \Swift_Message::newInstance()
                    ->setSubject('Contacto SOLINNOVACION.CO')
                    ->setFrom($this->container->getParameter('solinnovacion_email_from'))
                    ->setTo($entitie->getEmail())
                    ->setBody($this->renderView('BackendBundle:Contactenos:mailContact.html.twig', array(
                        'entitie' => $entitie,
                        'entity' => $entity)), 'text/html');
                        $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('contact_mail', array()));
//            return $this->render('BackendBundle:Cotizaciones:mailContact.html.twig', array(
//                        'entitie' => $entitie,
//                        'entity' => $entity));
        }

        return $this->render('BackendBundle:Contactenos:newRespuesta.html.twig', array(
                    'entity' => $entity,
                    'entitie' => $entitie,
                    'form' => $form->createView(),
        ));
    }

    public function deleteAction(Request $request) {
        $response['msg'] = '';
        $response['result'] = '__OK__';
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:ContactMail')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find cotización entity.');
        }

        $em->remove($entity);
        $em->flush();
        $response = new JsonResponse(
                array('result' => '__OK__'
        ));
        
        return $response;
    }

}
