<?php

namespace WebsiteControlPanel\BackendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WebsiteControlPanel\BackendBundle\Entity as Entity;


class UsersLoad implements FixtureInterface, ContainerAwareInterface
{

    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function load(ObjectManager $manager)
    {
        $users = array(
            array(
                'type' => Entity\User::USER_ADMINISTRATOR,
                'name' => 'Super Admin',
                'lastname' => '',
                'email' => 'gerencia@solinnovacion.com',
                'address' => '',
                'password' => 'solinnovacion321654987/*',
                ),
            array(
                'type' => Entity\User::USER_WEB_MANAGER,
                'name' => 'Web Manager',
                'lastname' => '',
                'email' => 'jmolina@kijho.com',
                'address' => '',
                'password' => 'kijho321654987/*',
                ),
        );

        foreach ($users as $item) {
            $User = new Entity\User();
            $User->setType($item['type']);
            $User->setNombre($item['name']);
            $User->setApellidos($item['lastname']);
            $User->setEmail($item['email']);
            $User->setSalt();

            $encoder = $this->container->get('security.encoder_factory')->getEncoder($User);
            $password = $encoder->encodePassword($item['password'], $User->getSalt());
            $User->setPassword($password);

            $manager->persist($User);
        }

        $manager->flush();
    }

}