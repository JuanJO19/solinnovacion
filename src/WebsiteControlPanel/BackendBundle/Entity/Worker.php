<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Raza
 * @ORM\Table(name="worker")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 15/05/2015
 */
class Worker {

    
    /**
     * @ORM\Id
     * @ORM\Column(name="worker_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="worker_name", type="string", length=255, nullable=false)
     */
    protected $name;  

    /**
     * Contrasena del usuario
     * @ORM\Column(name="worker_description", type="text", nullable=true)
     */
    protected $description;  
    
     /**
     * cargo que desempeña en la empresa
     * @ORM\Column(name="worker_position", type="string", nullable=true)
     */
    protected $position;
    
    /**
     * Instancia de un archivo de imagen para permitir la subida de la foto de la raza
     * @Assert\File(maxSize="4M", mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}, mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    protected $imagen;

    /**
     * Nombre de la imagen del trabajador
     * @ORM\Column(name="worker_image_path", type="string", length=100, nullable=true) 
     */
    protected $imagenPath;


    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getPosition() {
        return $this->position;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setPosition($position) {
        $this->position = $position;
    }
    
    public function getImagenPath() {
        return $this->imagenPath;
    }    
   
    public function setImagenPath($imagenPath) {
        $this->imagenPath = $imagenPath;
    }    
    
    public function getAvatar() {
        return $this->getImagePath();
    }
}
