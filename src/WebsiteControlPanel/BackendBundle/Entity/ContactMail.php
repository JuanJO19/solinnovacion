<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * tabla para almacenar los contactos para la pagina
 * @ORM\Table(name="contact_mail")
 * @ORM\Entity
 * @author Juan Molina <juanjosemolina19@gmail.com> 02/11/2015
 */
class ContactMail {

    /**
     * @ORM\Id
     * @ORM\Column(name="mail_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * name para identificar el usuario en el sistema
     * @ORM\Column(name="mail_user_name", type="string", length=255, nullable=false)
     */
    protected $nombreCompleto;

    /**
     * cantidad de cachorros de dicha raza    
     * @ORM\Column(name="mail_user_email", type="string", nullable=false)
     */
    protected $email;

    /**
     * Contrasena del usuario
     * @ORM\Column(name="mail_user_msg", type="text", nullable=false)
     */
    protected $mensaje;

    /**
     * Fecha de envio del mensaje
     * @ORM\Column(name="mail_send_date", type="datetime", nullable=true)
     */
    protected $creationDate;
    
    /**
     * foranea con respuesta para saber si fue respondida o no
     * @ORM\OneToOne(targetEntity="RespuestaMail", orphanRemoval=true)
     * @ORM\JoinColumn(name="conmail_resp_id", referencedColumnName="res_id")
     */
    protected $respuesta;

    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }

    function getNombreCompleto() {
        return $this->nombreCompleto;
    }

    function setNombreCompleto($nombreCompleto) {
        $this->nombreCompleto = $nombreCompleto;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEmail() {
        return $this->email;
    }

    function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;
    }

    function getCreationDate() {
        return $this->creationDate;
    }

    function getMensaje() {
        return $this->mensaje;
    }

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }

    function setEstado($telefono) {
        $this->telefono = $telefono;
    }
    
    function setRespuesta(RespuestaMail $respuesta = null) {
        $this->respuesta = $respuesta;
    }
    
    function getRespuesta() {
        return $this->respuesta;
    }

}
