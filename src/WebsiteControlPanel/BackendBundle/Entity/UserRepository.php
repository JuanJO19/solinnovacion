<?php

namespace WebsiteControlPanel\BackendBundle\Entity;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository{
  
   public function findPerson(){
      
      $em = $this->getEntityManager();
            
      $dql='SELECT u
        FROM  BackendBundle:User u
        JOIN u.person p WITH(u.person IS NOT NULL )
        ORDER BY u.id DESC';
      
      $consulta = $em->createQuery($dql);     
      return $consulta->getResult();     
         
   }  
   public function findCompany(){
      
      $em = $this->getEntityManager();
            
      $dql='SELECT u
        FROM  BackendBundle:User u
        JOIN u.company c WITH(u.company IS NOT NULL )
        ORDER BY u.id DESC';
      
      $consulta = $em->createQuery($dql);     
      return $consulta->getResult();     
         
   }  
      
}
