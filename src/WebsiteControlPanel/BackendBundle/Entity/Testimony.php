<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * tabla para almacenar los testimonios
 * @ORM\Table(name="testimony")
 * @ORM\Entity
 * @author Juan Molina <juanjosemolina19@gmail.com> 02/11/2015
 */
class Testimony {

    /**
     * @ORM\Id
     * @ORM\Column(name="test_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * name para identificar el usuario en el sistema
     * @ORM\Column(name="test_name", type="string", length=255, nullable=false)
     */
    protected $nombreCompleto;

    /**
     * cantidad de cachorros de dicha raza
     * @Assert\Email(checkMX=true)
     * @ORM\Column(name="test_email", type="string", nullable=false)
     */
    protected $email;

    /**
     * Contrasena del usuario
     * @ORM\Column(name="test_msg", type="text", nullable=true)
     */
    protected $mensaje;

    /**
     * Estado del testimonio, como aprobado o desaprobado
     * @ORM\Column(name="test_estado", type="boolean", nullable=true)
     */
    protected $estado;
    
     /**
     * Orden del testimonio
     * @ORM\Column(name="test_order", type="integer", nullable=true)
     */
    protected $order;

    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }

    function getNombreCompleto() {
        return $this->nombreCompleto;
    }

    function setNombreCompleto($nombreCompleto) {
        $this->nombreCompleto = $nombreCompleto;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function getEmail() {
        return $this->email;
    }
    
    function getMensaje() {
        return $this->mensaje;
    }

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }

    function getEstado() {
        return $this->estado;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    function getOrder() {
        return $this->order;
    }

    function setOrder($order) {
        $this->order = $order;
    }

}
