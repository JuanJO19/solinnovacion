<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * contenido del index
 * @ORM\Table(name="content")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 15/05/2015
 */
class Content {

    /**
     * Constantes para los tipos contenidos de la unipagina
     */
    const MISION = 0;
    const VISION = 1;
    const SERVICIOS_REDES_ELECTRICAS = 2;
    const SERVICIOS_OBRAS_CIVILES = 3;
    const SERVICIOS_ORGANIZACIONALES = 4;
    const INTRO_MISION_Y_VISION = 5;
    const INTRO_ACERCA_DE_NOSOTROS = 6;
    const INTRO_PORTAFOLIO = 7;
    const INTRO_NUESTROS_SERVICIOS = 8;
    const INTRO_CONTACTENOS = 9;

    /**
     * @ORM\Id
     * @ORM\Column(name="cont_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Tipo del contenido
     * @ORM\Column(name="cont_tipo", type="integer", nullable=false)
     */
    protected $type;

    /**
     * Contenido para la seccion respectiva
     * @ORM\Column(name="cont_content", type="text", nullable=true)
     */
    protected $content;

    public function getTextTipo($type = null) {
        $text = '';

        if (!$type) {
            $type = $this->type;
        }

        switch ($type) {
            case self::MISION: $text = 'Misión';
                break;
            case self::VISION: $text = 'Visión';
                break;
            case self::SERVICIOS_REDES_ELECTRICAS: $text = 'Servicios de redes electricas';
                break;
            case self::SERVICIOS_OBRAS_CIVILES: $text = 'Servicios de obras civiles';
                break;
            case self::SERVICIOS_ORGANIZACIONALES: $text = 'Servicios Organizacionales';
                break;
            case self::INTRO_MISION_Y_VISION: $text = 'Encabezado de Misión y Visión';
                break;
            case self::INTRO_ACERCA_DE_NOSOTROS: $text = 'Encabezado Acerca de Nosotros';
                break;
            case self::INTRO_PORTAFOLIO: $text = 'Encabezado Portafolio';
                break;
            case self::INTRO_NUESTROS_SERVICIOS: $text = 'Encabezado Nuestros servicios';
                break;
            case self::INTRO_CONTACTENOS: $text = 'Encabezado Contactenos';
                break;
        }
        return $text;
    }

    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }

    function getContent() {
        return $this->content;
    }

    function setContent($content) {
        $this->content = $content;
    }

}
