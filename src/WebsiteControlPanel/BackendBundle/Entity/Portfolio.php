<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use AdminLTE\AdminThemeBundle\Model\UserInterface as adminLTEUserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * clientes de solinnovacion para ser mostrados en la pagina inicial
 * @ORM\Table(name="portfolio")
 * @ORM\Entity
 * @author Juan Molina <jmolina@kijho.com> 15/05/2015
 */
class Portfolio {

      /**
     * Constantes para los tipos de proyectos
     */
    const REDES_ELECTRICAS = 0;
    const OBRAS_CIVILES = 1;
    const SERVICIOS_ORGANIZACIONALES = 2;
    
    
    
    /**
     * @ORM\Id
     * @ORM\Column(name="port_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Nick name para identificar el usuario en el sistema
     * @ORM\Column(name="port_name", type="string", length=255, nullable=false)
     */
    protected $name; 
    
    /**
     * Tipo del proyecto
     * @ORM\Column(name="raza_tipo", type="integer", nullable=false)
     */
    protected $type;

    /**
     * Contrasena del usuario
     * @ORM\Column(name="port_description", type="text", nullable=true)
     */
    protected $description;  
  
    
    /**
     * Instancia de un archivo de imagen para permitir la subida de la foto de la raza
     * @Assert\File(maxSize="4M", mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}, mimeTypesMessage = "Extensión de archivo inválida (.PNG - .JPEG - .PJPEG)")
     */
    protected $imagen;

    /**
     * Nombre de la imagen del trabajador
     * @ORM\Column(name="port_image_path", type="string", length=100, nullable=true) 
     */
    protected $imagenPath;
    
    public function getTextTipo($type = null) {
        $text = '';

        if (!$type) {
            $type = $this->type;
        }
        
        switch ($type) {
            case self::REDES_ELECTRICAS: $text = 'Redes Electricas';
                break;
            case self::OBRAS_CIVILES: $text = 'Obras Civiles';
                break;
            case self::SERVICIOS_ORGANIZACIONALES: $text = 'Servicios Organizacionales';
                break;
        }
        return $text;
    }

    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }
    
    public function serialize() {
        return serialize(array(
            $this->id,
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->id,
                ) = unserialize($serialized);
    }

    function getId() {
        return $this->id;
    }  
    
    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }    

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    public function getImagenPath() {
        return $this->imagenPath;
    }    
   
    public function setImagenPath($imagenPath) {
        $this->imagenPath = $imagenPath;
    }    
    
    public function getAvatar() {
        return $this->getImagePath();
    }
}
