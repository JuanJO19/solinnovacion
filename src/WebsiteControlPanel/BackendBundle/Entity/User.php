<?php

namespace WebsiteControlPanel\BackendBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;


/**
* @ORM\Table(name="user",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_user_email", columns={"user_email"})})
 *@ORM\Entity
 */
class User implements UserInterface  {
    
    //Tipos de usuarios
    const USER_ADMINISTRATOR     = 1;
    const USER_WEB_MANAGER       = 2;
  
    /**
     * @ORM\Id
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    
    /** @ORM\Column(name="user_name", type="string", length=100) */
    protected $name;
    
     /** @ORM\Column(name="user_type", type="integer") */
    protected $type;
    
    /** @ORM\Column(name="user_lastName", type="string", length=100, nullable=true) */
    protected $lastName;
    
    /** @ORM\Column(name="user_email", type="string", length=255) */
    protected $email;
    
    /** @ORM\Column(name="user_password", type="string", length=255) */
    protected $password;
    
    /** @ORM\Column(name="user_address", type="text", nullable=true) */
    protected $address;
    
    /** @ORM\column(name="user_salt", type="text")*/ 
    protected $salt;
    
   
    
    
    public function getId() {
       return $this->id;
    }

    public function getName() {
       return $this->name;
    }

    public function getLastName() {
       return $this->lastName;
    }

    public function getEmail() {
       return $this->email;
    }

    public function getPassword() {
       return $this->password;
    }

    public function setNombre($name) {
       $this->name = $name;
    }

    public function setApellidos($lastName) {
       $this->lastName = $lastName;
    }

    public function setEmail($email) {
       $this->email = $email;
    }

    public function setPassword($password) {
       $this->password = $password;
    }

   
    public function setDireccion($address) {
       $this->address = $address;
    }

   public function eraseCredentials() {
      
   }

   public function getSalt() {
      return $this->salt;
   }

   public function getUsername() {
      return $this->getEmail();
   }
   
   public function getType() {
       return $this->type;
   }

   public function setType($type) {
       $this->type = $type;
   }

   public function setSalt() {
      mt_srand();
      $this->salt = md5(time().mt_rand());
   }

   
   public function getRoles() 
   {
        if($this->type == self::USER_ADMINISTRATOR) {
            return array('ROLE_USUARIO'); 
        }
        elseif($this->type == self::USER_WEB_MANAGER) {
            return array('ROLE_WEB_MANAGER');
        }
        else {
            return array('ROLE_INACTIVE');
        }
    }

   

}
