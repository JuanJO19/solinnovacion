<?php

namespace WebsiteControlPanel\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebsiteControlPanel\BackendBundle\Entity\View;
use WebsiteControlPanel\BackendBundle\Entity\ContactoCotizar;
use WebsiteControlPanel\BackendBundle\Entity\Testimony;
use WebsiteControlPanel\BackendBundle\Entity\ContactMail;
use WebsiteControlPanel\BackendBundle\Form\ContactoCotizarType;
use WebsiteControlPanel\BackendBundle\Form\TestimonyType;
use WebsiteControlPanel\BackendBundle\Form\ContactMailType;
use WebsiteControlPanel\BackendBundle\Util\Util;

/**
 * @author Juan José Molina <jmolina@kijho.com>
 */
class DefaultController extends Controller {

    /**
     * index de la pagina.
     * @return type
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        // instancia de la entidad 
        $entity = new ContactMail;
        //formulario para mandar el email
        $formMail = $this->createForm(new ContactMailType(), $entity);
        //array de testimonios con filtro de aprobados y en orden
        $testimony = $em->getRepository('BackendBundle:Testimony')->findBy(
                array('estado' => true), array('order' => 'ASC'), 6, 0);
        //array de clientes
        $clients = $em->getRepository('BackendBundle:Client')->findAll();
        //array de trabajadores
        $workers = $em->getRepository('BackendBundle:Worker')->findAll();
        //array de portafolio
        $portfolios = $em->getRepository('BackendBundle:Portfolio')->findAll();
        //contenidos creados desde el backend
        $mision = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 0));
        $vision = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 1));
        $servicios_r_e = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 2));
        $servicios_o_c = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 3));
        $servicios_o = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 4));
        $intro_m_v = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 5));
        $intro_a_n = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 6));
        $intro_port = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 7));
        $intro_n_s = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 8));
        $intro_contact = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 9));
        return $this->render('FrontendBundle:Default:index.html.twig', array(
                    'testimonios' => $testimony,
                    'formMail' => $formMail->createView(),
                    'clients' => $clients,
                    'workers' => $workers,
                    'portfolios' => $portfolios,
                    'mision' => $mision,
                    'vision' => $vision,
                    'servicios_r_e' => $servicios_r_e,
                    'servicios_o_c' => $servicios_o_c,
                    'servicios_o' => $servicios_o,
                    'intro_m_v' => $intro_m_v,
                    'intro_a_n' => $intro_a_n,
                    'intro_port' => $intro_port,
                    'intro_n_s' => $intro_n_s,
                    'intro_contact' => $intro_contact,
        ));
    }

    /**
     * metodo para hacer el render del fancy para testimonios
     * @return type
     */
    public function fancyTestiAction() {

        $entity = new Testimony;
        $form = $this->createForm(new TestimonyType(), $entity);

        return $this->render('FrontendBundle:Default:fancyTestimonio.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * metodo para hacer el render del fancy para testimonios
     * @return type
     */
    public function fancyEnergiaElectricaAction() {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 2));
        $title = 'Energía Eléctrica';

        return $this->render('FrontendBundle:Default:fancyListadoServicios.html.twig', array(
                    'entity' => $entity,
                    'title' => $title,
        ));
    }

    /**
     * metodo para hacer el render del fancy para testimonios
     * @return type
     */
    public function fancyObrasCivilesAction() {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 3));
        $title = 'Obras Civiles';

        return $this->render('FrontendBundle:Default:fancyListadoServicios.html.twig', array(
                    'entity' => $entity,
                    'title' => $title,
        ));
    }

    /**
     * metodo para hacer el render del fancy para testimonios
     * @return type
     */
    public function fancyServiciosOrganizacionalesAction() {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Content')->findOneBy(array('type' => 4));
        $title = 'Servicios Organizacionales';

        return $this->render('FrontendBundle:Default:fancyListadoServicios.html.twig', array(
                    'entity' => $entity,
                    'title' => $title,
        ));
    }

    public function fancyPortfolioAction($type) {
        $em = $this->getDoctrine()->getManager();
        if ($type == 0) {
            $portfolio = $em->getRepository('BackendBundle:Portfolio')->findBy(array('type' => \WebsiteControlPanel\BackendBundle\Entity\Portfolio::REDES_ELECTRICAS));
        } elseif ($type == 1) {
            $portfolio = $em->getRepository('BackendBundle:Portfolio')->findBy(array('type' => \WebsiteControlPanel\BackendBundle\Entity\Portfolio::OBRAS_CIVILES));
        } else {
            $portfolio = $em->getRepository('BackendBundle:Portfolio')->findBy(array('type' => \WebsiteControlPanel\BackendBundle\Entity\Portfolio::SERVICIOS_ORGANIZACIONALES));
        }

        return $this->render('FrontendBundle:Default:fancyPortfolio.html.twig', array(
                    'portfolios' => $portfolio
        ));
    }

    public function fancyTestimonioAddAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $entity = new Testimony;
        $form = $this->createForm(new TestimonyType(), $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity->setEstado(0);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('notice', 'Gracias por tu aporte, pronto será publicado!');
        }

//        return $this->render('FrontendBundle:Default:fancyTestimonio.html.twig', array(
//                    'entity' => $entity,
//                    'form' => $form->createView(),
//        ));
        return $this->redirect($this->generateUrl('frontend_fancy_testimonio', array()));
    }

    public function fancyCondicionesViewAction() {

        return $this->render('FrontendBundle:Default:fancyCondiciones.html.twig', array(
        ));
    }

    public function fullGalleryAction() {

        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository('BackendBundle:GalleryImages')->findAll();
        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:fullGallery.html.twig', array(
                    'galeria' => $gallery,
                    'formMail' => $formMail->createView(),
        ));
    }

    public function nosotrosAction() {

        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:nosotros.html.twig', array(
                    'formMail' => $formMail->createView(),
        ));
    }

    /**
     * metodo para contar las visitas en la pagina
     * @author Juan José Molina <jmolina@kijho.com>
     * @since 1.0 08/11/2015
     * @return \WebsiteControlPanel\FrontendBundle\Controller\Response
     */
    public function newPageViewAction() {

        $em = $this->getDoctrine()->getManager();
        $view = new View();
        $views = $em->getRepository('BackendBundle:View')->findAll();
        $total = count($views) + 1;
        $view->setTotal($total);
        $em->persist($view);
        $em->flush();
        $response['result'] = '__OK__';
        $r = new Response(json_encode($response));
        $r->headers->set('Content-Type', 'application/json');

        return $r;
    }

    /**
     * metodo para generar las vistas de cotizar razas con sus respectivos tipos
     * @author Juan José Molina <jmolina@kijho.com>
     * @since 1.0 08/11/2015
     * @return \WebsiteControlPanel\FrontendBundle\Controller\Response
     */
    public function cotizarRazasAction() {

        $em = $this->getDoctrine()->getManager();
        $razasPequeñas = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 0));
        $razasMedianas = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 1));
        $razasGrandes = $em->getRepository('BackendBundle:Raza')->findBy(array('tipo' => 2));
        $entity = new ContactMail;
        $formMail = $this->createForm(new ContactMailType(), $entity);

        return $this->render('FrontendBundle:Default:cotizarRazas.html.twig', array(
                    'razasPequeñas' => $razasPequeñas,
                    'razasMedianas' => $razasMedianas,
                    'razasGrandes' => $razasGrandes,
                    'formMail' => $formMail->createView(),
        ));
    }

    /**
     * Metodo para abrir fancy y de esta foram enviar correo adjuntando hoja de vida
     * @param Request $request
     * @return type
     */
    public function contactAction() {

        $em = $this->getDoctrine()->getManager();

        //Formulario de Worker
        $contact = new ContactMail();
        $formMail = $this->createForm(new ContactMailType, $contact);
        $request = $this->getRequest();
        $formMail->handleRequest($request);
        $flag = false;
        //Validación de la información enviada en el formulario
        if ($formMail->isValid()) {

            //Asignación fecha-hora de envio correo y guardado en base de datos               
            $contact->setCreationDate(Util::getCurrentDate());
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $flag = true;
//                $emailFrom = $contact->getEmail();
            //Llenado de variable para enviar correo
//                $msg = \Swift_Message::newInstance()
//                        ->setSubject('Contactenos - PUPPYHOME.CO')
//                        ->setFrom($emailFrom)
//                        ->setTo($this->container->getParameter('puppyhome_email_to'))
//                        ->setBody($this->renderView('FrontendBundle:Default:mailContact.html.twig', array(
//                            'contact' => $contact)), 'text/html');
//                //Envio de correo y notificación de éxito
//                $this->get('mailer')->send($msg);               

            return $this->redirect($this->generateUrl('frontend_homepage', array('flag' => $flag)));
        }


        return $this->render('FrontendBundle:Default:index.html.twig', array(
                    'formMail' => $formMail->createView(),
        ));
    }

}
